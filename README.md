# HTML Parser

html-parser is a command line tool to extract links from HTML documents (e.g., from local files or URLs).

## Requirements

- Python >=3.6
- beautifulsoup4 >=4.4

You can install the required dependencies via: `pip install -r requirements.txt`

## Usage

Run the tool as follows:

```
> export PYTHONPATH=.
> python -m html_parser.main <URL_or_LOCAL_HTML_FILE>
```

For more information, please see the help message:

```
General Usage:

html-parser [PATH TO HTML DOCUMENT]
It currently parses a HTML document and prints all links.

Example local file:   html-parser /home/user/test.html
Example HTTP:         html-parser https://www.google.de/
```

## Testing

We require additional dependencies for testing.
You can install them via: `pip install -r requirements-dev.txt`

Then, you can run the available tests as follows:

```
> export PYTHONPATH=. # Make sure that the html_parser module is found.
> pytest tests/
```

In addition, you can check the achieved code coverage as follows:

`pytest --cov=html_parser --cov-report=term-missing --cov-branch tests/`

If you add the option `-cov-report=html`, a HTML coverage report will be created in the directory `htmlcov`.

Finally, you can select the tests to run, for example, as follows:
- Run small tests only: `pytest -m "not large and not medium" tests/`
- Run medium tests only: `pytest -m "medium" tests/`
- Run large tests only: `pytest -m "large" tests/`
